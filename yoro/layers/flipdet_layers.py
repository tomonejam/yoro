import torch
from torch.nn import Module, Sequential, Flatten, Linear
from torch.nn import functional as F

from .rotdetect_layers import get_degrees


class FlipDetect(Module):

    __constants__ = ['width', 'height']

    def __init__(self, in_features: int, width: int, height: int):

        super(FlipDetect, self).__init__()

        # Save parameters
        self.width = width
        self.height = height

        # Build flip classifier
        self.classifier = Sequential(Flatten(), Linear(in_features, 2))

    @torch.jit.export
    def decode(self, inputs):
        return torch.argmax(inputs, dim=1)

    def forward(self, inputs):
        inputs = self.classifier(inputs)
        return self.decode(inputs)

    @torch.jit.unused
    def loss(self, inputs, targets):

        device = inputs.device
        dtype = inputs.dtype

        # Predict
        inputs = self.classifier(inputs)
        predict = self.decode(inputs)

        # Build target
        targets = (torch.cos(torch.deg2rad(
            get_degrees(targets, dtype, device))) >= 0).to(torch.long)

        # Find loss
        loss = F.cross_entropy(inputs, targets)
        acc = torch.mean((predict == targets).to(torch.float))

        return ((loss, 1), {'acc': (acc.item(), 1)})
