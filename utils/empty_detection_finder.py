#!@Python_EXECUTABLE@

import argparse
import cv2 as cv
from os.path import join, basename, splitext

from glob import glob
from tqdm import tqdm

from yoro.api import YORODetector, DeviceType

device = {
    'auto': DeviceType.Auto,
    'cuda': DeviceType.CUDA,
    'cpu': DeviceType.CPU
}


if __name__ == '__main__':

    # Parse arguments
    argp = argparse.ArgumentParser(
        description='Find images with zero detections')

    argp.add_argument('model', type=str, help='YORO model path')
    argp.add_argument('image_folder', type=str, help='Source image folder')
    argp.add_argument('output', type=str, help='Output path for record file')

    opt = argp.add_argument_group('Optional program arguments')
    opt.add_argument('--device', type=str,
                     choices=device.keys(), default='auto',
                     help='Inference on specific device. (default: %(default)s)')
    opt.add_argument('--conf', type=float, default=0.5,
                     help=('Confidence filtering threshold. (default: %(default)s)'))
    opt.add_argument('--nms', type=float, default=0.6,
                     help=('NMS filtering threshold. (default: %(default)s)'))

    args = argp.parse_args()

    # Load model
    print('Loading model from:', args.model)
    detector = YORODetector(args.model, device.get(args.device, None))

    # Build record file
    outList = []
    imgList = glob(join(args.image_folder, '*.jpg'))
    for imgPath in tqdm(imgList):

        img = cv.imread(imgPath, cv.IMREAD_COLOR)
        results = detector.detect(img, args.conf, args.nms)
        if len(results) == 0:
            outList.append(imgPath)

    for path in outList:
        print(path)
    print()

    # Write record file
    print('Dump record to', args.output)
    with open(args.output, 'w') as f:
        for path in outList:
            f.write(path + '\n')
