#!@Python_EXECUTABLE@

import argparse
import yaml
import math

import numpy as np
import cv2 as cv
import pandas as pd

from os.path import join, basename, splitext, exists

from glob import glob
from tqdm import tqdm

from yoro.api import YORODetector, DeviceType

device = {
    'auto': DeviceType.Auto,
    'cuda': DeviceType.CUDA,
    'cpu': DeviceType.CPU
}


if __name__ == '__main__':

    # Parse arguments
    argp = argparse.ArgumentParser(
        description='Generate ROI Annotation by YORO Model')

    argp.add_argument('model', type=str, help='YORO model path')
    argp.add_argument('image_folder', type=str, help='Source image folder')
    argp.add_argument('src_anno', type=str, help='Source annotation csv file')
    argp.add_argument('output', type=str, help='Output annotation csv path')

    opt = argp.add_argument_group('Optional program arguments')
    opt.add_argument('--device', type=str,
                     choices=device.keys(), default='auto',
                     help='Inference on specific device. (default: %(default)s)')
    opt.add_argument('--conf-scan', type=float, nargs='+',
                     default=[0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1],
                     help=('Confidence filtering threshold. (default: %(default)s)'))
    opt.add_argument('--nms', type=float, default=0.6,
                     help=('NMS filtering threshold. (default: %(default)s)'))

    args = argp.parse_args()

    # Load model
    print('Loading model from:', args.model)
    detector = YORODetector(args.model, device.get(args.device, None))

    # Load external annotation
    anno = pd.read_csv(args.src_anno)
    fileList = list(anno['filename'])
    labelList = list(anno['label'])

    filename = []
    label = []

    trX = []
    trY = []
    brX = []
    brY = []
    blX = []
    blY = []
    tlX = []
    tlY = []

    rboxConf = []
    rboxDeg = []
    rboxX = []
    rboxY = []
    rboxW = []
    rboxH = []

    reverse = []

    # Build annotation dictionary
    for (fn, text) in tqdm(zip(fileList, labelList), total=len(fileList)):

        # Predict
        imgPath = join(args.image_folder, fn + '.jpg')
        img = cv.imread(imgPath, cv.IMREAD_COLOR)
        results = []
        for conf_th in args.conf_scan:
            results = detector.detect(img, conf_th, args.nms)
            if len(results) > 0:
                break

        if len(results) == 0:
            print('Warning: no detections for', imgPath)
            annoPath = imgPath + '.mark'
            if exists(annoPath):
                print('Using manual annotation:', annoPath)
                print()
                with open(annoPath, 'r') as f:
                    det = yaml.load(f, Loader=yaml.FullLoader)[0]
            else:
                print('No manual annotation was found. Drop this sample.')
                continue

        else:
            det = results[0]
            for result in results:
                if result.conf > det.conf:
                    det = result
            det = det.to_dict()

        # Record for filename and label
        filename.append(fn)
        label.append(text)

        # Record for rotated bounding box information
        th = det['degree']
        x = det['x']
        y = det['y']
        w = det['w']
        h = det['h']

        rboxConf.append(det.get('conf', 1.0))
        rboxDeg.append(th)
        rboxX.append(x)
        rboxY.append(y)
        rboxW.append(w)
        rboxH.append(h)

        # Convert annotation to original format
        reverseAttr = False
        if math.cos(math.radians(th)) < 0:
            th = th + 180
            reverseAttr = True
        rad = math.radians(th)

        sinVal = math.sin(rad)
        cosVal = math.cos(rad)
        rotMat = np.float32([
            [cosVal, -sinVal],
            [sinVal, cosVal]
        ])

        origMat = np.float32([x, y])

        pts = np.zeros((4, 2), dtype=np.float32)
        pts[0] = np.matmul(np.float32([+w / 2, -h / 2]), rotMat) + origMat
        pts[1] = np.matmul(np.float32([+w / 2, +h / 2]), rotMat) + origMat
        pts[2] = np.matmul(np.float32([-w / 2, +h / 2]), rotMat) + origMat
        pts[3] = np.matmul(np.float32([-w / 2, -h / 2]), rotMat) + origMat

        reverse.append(reverseAttr)

        trX.append(pts[0][0])
        trY.append(pts[0][1])
        brX.append(pts[1][0])
        brY.append(pts[1][1])
        blX.append(pts[2][0])
        blY.append(pts[2][1])
        tlX.append(pts[3][0])
        tlY.append(pts[3][1])

    data = pd.DataFrame({
        'filename': filename,
        'label': label,

        'top right x': trX,
        'top right y': trY,
        'bottom right x': brX,
        'bottom right y': brY,
        'bottom left x': blX,
        'bottom left y': blY,
        'top left x': tlX,
        'top left y': tlY,

        'rbox conf': rboxConf,
        'rbox degree': rboxDeg,
        'rbox x': rboxX,
        'rbox y': rboxY,
        'rbox w': rboxW,
        'rbox h': rboxH,

        'reverse': reverse
    })

    print('Writting annotations to:', args.output)
    data.to_csv(args.output, index=False)
